//
//  Extension.swift
//  Pinterest
//
//  Created by Narasimha Poluparthi on 04/09/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIImageView

extension UIImageView {
    func roundview(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: CGColor) {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor
    }
}
