//
//  ListCell.swift
//  Pinterest
//
//  Created by Narasimha Poluparthi on 04/09/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit

class ListCell: UICollectionViewCell {
    
    @IBOutlet  weak var containerView: UIView!
    @IBOutlet  weak var imageView: CustomImageView!
    @IBOutlet  weak var userNameLabel: UILabel!
    @IBOutlet  weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 6
        containerView.layer.masksToBounds = true
    }
    
    var list: Post? {
        didSet {
            if let list = list {
                imageView.loadImageUsingUrlString(urlString: list.user?.profile_image?.large ?? "")
                userNameLabel.text = list.user?.username
                nameLabel.text = list.user?.name
            }
        }
    }
    
}
