//
//  ViewController.swift
//  Pinterest
//
//  Created by Narasimha Poluparthi on 04/09/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit

class ListViewController: UICollectionViewController {

    var posts: [Post] = []
    var refresher:UIRefreshControl!

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUP()
       fetchPosts()
    }
    
    private func fetchPosts() {
        if Utlis.shared.internetcheck() {
            self.fetchPostsFromApi()
        }else {
            Utlis.shared.alert(msg: "Please check you internet connection")
            stopRefresher()
        }
    }
    private func setUP() {
        collectionView?.contentInset = UIEdgeInsets(top: 23, left: 16, bottom: 10, right: 16)
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    @objc private func loadData() {
       fetchPosts()
    }
    
    private func stopRefresher() {
        DispatchQueue.main.async {
            self.refresher.endRefreshing()
        }
    }
    
    private func fetchPostsFromApi() {
        
        Api.shared.fetchPosts { result in
            switch result {
            case .success(let posts):
                debugPrint(posts)
                self.posts = posts
                DispatchQueue.main.async {
                self.collectionView.reloadData()
                }
            case .failure:
                debugPrint("FAILED")
            }
             self.stopRefresher()
        }
    }
}

extension ListViewController: UICollectionViewDelegateFlowLayout {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCell", for: indexPath as IndexPath) as! ListCell
        cell.list = posts[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 2
        return CGSize(width: itemSize, height: itemSize)
    }
    
}
