//
//  Api.swift
//  Pinterest
//
//  Created by Narasimha Poluparthi on 04/09/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation

class Api {
    
    static let shared: Api = Api()
    let url = URL(string: "http://pastebin.com/raw/wgkJgazE")!

    func fetchPosts(completion: @escaping (Result<[Post],Error>) -> Void) {
        
        let task = URLSession.shared.dataTask(with: self.url) { (data, response, error) in
            
            guard let data = data, error == nil else {
                if let error = error as NSError?, error.domain == NSURLErrorDomain {
                    completion(.failure(error))
                }
                return
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                let posts = try jsonDecoder.decode([Post].self, from: data)
                completion(.success(posts))
            } catch {
                completion(.failure(error))
            }
           
            
        }
        task.resume()
        
//        URLSession.shared.dataTask(with: self.url) { data, response, error in
//
//            guard let data = data, error == nil else {
//                if let error = error as NSError?, error.domain == NSURLErrorDomain {
//                    completion(.failure(error))
//                }
//                return
//            }
//
//            do {
//                let posts = try JSONDecoder().decode([Posts].self, from: data)
//                completion(.success(posts))
//            } catch {
//                completion(.failure(error))
//            }
//
//            }.resume()
        
    }
}
