//
//  Utlis.swift
//  Pinterest
//
//  Created by Narasimha Poluparthi on 05/09/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation
import UIKit

class Utlis {
    
    static let shared: Utlis = Utlis()

    func internetcheck() -> Bool {
        
        let status = Reachability.shared.connectionStatus()
        
        if (status == .online)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func alert(msg:String)  {
        
        let alertController = UIAlertController(title: "Pinterest", message: msg, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            (result : UIAlertAction) -> Void in
            debugPrint("You pressed OK")
        }
        alertController.addAction(okAction)
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.windowLevel = UIWindow.Level.alert
        alertWindow.rootViewController = UIViewController()
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
