//
//  PinterestTests.swift
//  PinterestTests
//
//  Created by Narasimha Poluparthi on 04/09/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import XCTest
@testable import Pinterest

class PinterestTests: XCTestCase {

    var listView: ListViewController!
    var listCell: ListCell!

    override func setUp() {
        listView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListViewController") as? ListViewController
        listCell = listView.collectionView.dequeueReusableCell(withReuseIdentifier: "ListCell", for: IndexPath(item: 0, section: 0)) as? ListCell
    }

    override func tearDown() {
        listView = nil
        listCell = nil
    }

    func testOutlets() {
        XCTAssertNotNil(listCell.containerView)
        XCTAssertNotNil(listCell.imageView)
        XCTAssertNotNil(listCell.userNameLabel)
        XCTAssertNotNil(listCell.nameLabel)
    }
    
    func testValidCallTofetchPostsGetsSuccess() {
        let promise = expectation(description: "Fetch posts success")
        Api.shared.fetchPosts { result in
            switch result {
            case .success(_):
                promise.fulfill()
            case .failure:
                 XCTFail("Fetch posts failed")
            }
        }
        waitForExpectations(timeout: 30, handler: nil)
    }
}
